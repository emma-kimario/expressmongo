var express = require('express');
var router = express.Router();

/* GET home page. */
var pageTitle = "expressMongo";
router.get('/', function(req, res, next) {
  var db = req.db;
  var collection = db.get('usercollection');
  collection.find({}, {}, function(e, data){
    console.log(data);
    res.render('index', { 
      pageTitle: pageTitle,
      kimarioData: data
    });
  });
});



module.exports = router;
